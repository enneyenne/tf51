output "nat-ip-address" {
  value = yandex_compute_instance.vm-instance.*.network_interface.0.nat_ip_address
}

output "vm-name" {
  value = yandex_compute_instance.vm-instance.*.name
}