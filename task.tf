variable "private_key" {
}

resource "yandex_compute_instance" "vm-instance" {

  connection {
    user        = "eneyne"
    type        = "ssh"
    timeout     = "5m"
    host        = self.network_interface.0.nat_ip_address
    private_key = file(var.private_key)
  }

  provisioner "local-exec" {
    command = "echo ${self.name} ${self.network_interface.0.nat_ip_address} >> host1.list"
  }

  provisioner "file" {
    source      = "host1.list"
    destination = "/tmp/host1.list"
  }

  provisioner "remote-exec" {
    script = "bashscript.sh"
  }

  metadata = {
    user-data = "${file("cloud-init.yml")}"
  }

  count     = 3
  name      = "terraform-vm-${count.index + 1}"
  folder_id = var.yc_folder
  zone      = var.yc_zone

  resources {
    core_fraction = 20
    cores         = 2
    memory        = 1
  }

  boot_disk {
    initialize_params {
      image_id = "fd84aafrovb77mh7vj51"
      size     = 10
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }

  scheduling_policy {
    preemptible = true
  }

  lifecycle {
    create_before_destroy = true
  }

}

resource "yandex_vpc_network" "network-1" {
  name = "network1"
}

resource "yandex_vpc_subnet" "subnet-1" {
  name           = "subnet1"
  zone           = "ru-central1-b"
  network_id     = yandex_vpc_network.network-1.id
  v4_cidr_blocks = ["192.168.0.0/24"]
}