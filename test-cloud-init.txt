#cloud-config
package_update: true

packages:
  - nginx

write_files:
- path: /usr/share/nginx/html/index.html
  content: | 
    juwe1
    juwe2
  defer: true

runcmd:
  - service nginx start

users:
  - name: eneyne
    groups: sudo
    shell: /bin/bash
    sudo: ['ALL=(ALL) NOPASSWD:ALL']
    ssh-authorized-keys:
      - del