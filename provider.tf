terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
    cloudinit = {
      source = "hashicorp/cloudinit"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  zone      = var.yc_zone
  cloud_id  = var.yc_cloud
  folder_id = var.yc_folder
  token     = var.yc_token
}